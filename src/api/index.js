import { Router } from 'express';
import homeRoute from './routes/homeRoute';
import productRoute from './routes/productRoute';
import sendGreetingRoute from "./routes/sendGreetingRoute";
import authRouter from "./routes/authRouter";
import cityRouter from "./routes/cityRouter";

export default () => {
	const app = Router();

	homeRoute(app);
	productRoute(app);
	sendGreetingRoute(app);
	authRouter(app);
	cityRouter(app);

	return app;
}
