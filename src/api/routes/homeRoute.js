import {Router} from 'express';
import config from '../../config';

const route = Router();

const menu = [
	{label: 'Home', url: '/'},
	{label: 'Products', url: '/products'},
	{label: 'About', url: '/about'},
];

const adminMenu = [
	...menu,
	...[
		{label: 'Manage Products', url: '/manage'},
	]
]

export default app => {
	app.use('/', route);

	route.get('/admin', (request, response) => {
		response.render('admin', {menu: adminMenu});
	});

	route.get('/about', (request, response) => {
		response.render('about', {menu});
	});

	route.get('/', (request, response) => {


		response.render('index', {
			friends: 10,
			user: {
				name: 'Pavel',
				saldo: 1234.56
			},
			menu
		});
	});

	route.get('/who-are-you', (request, response) => {
		response.send(config.DEV_ID);
	});
}