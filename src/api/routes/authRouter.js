import {Router} from 'express';
import UserService from "../../services/UserService";
import {Joi, celebrate} from "celebrate";

const route = Router();

export default app => {
  app.use('/auth', route);

  route.get('/get-user/:userId',
    async(request, response, next) => {
      try {
        const {userId} = request.params;
        const user = await UserService.getUserById(userId);

        return {

        }
      } catch (e) {
        next(e)
      }
    });

  route.post('/signup',
    celebrate({
      body: Joi.object().keys({
        name: Joi
              .string()
              .required()
              .error(new Error('Name is required')),
        email: Joi
              .string()
              .email()
              .required()
              .error(new Error('Email is required')),
        password: Joi
          .string()
          .required()
          .error(new Error('Password is required'))
      }
      )}));

  route.post('/', async (request, response) => {
    const {name, email} = request.body;

    const user = await UserService.createUser({
      name, email
    });



    return response.json({
      success: true,
      data: user
    })
  });
}