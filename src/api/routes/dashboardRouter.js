import {Router} from 'express';
import config from '../../config';

const route = Router();

export default app => {
  app.use('/dashboard', route);

  /**
   * @desc роут має бути лише для авторизованих користувачів, тобто необхідно длодати мідлвер длля перевірки токену
   */
  route.get('/list', async (request, response) => {
    return response.json({
      success: true,
      data: []
    })
  });
}