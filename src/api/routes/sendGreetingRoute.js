import {Router} from 'express';
import config from '../../config';
import mailgun from 'mailgun-js';

const route = Router();

export default app => {
	app.use('/mail', route);

	route.post('/welcome-new-dev', (request, response) => {

		try {
			const sendMail = mailgun({
				apiKey: config.MAILGUN_KEY,
				domain: config.MAILGUN_DOMAIN
			});

			var data = {
				from: 'Excited User <noresponse@my.domain.com>',
				to: 'telezhenkopv@gmail.com',
				subject: 'Hello my dear me!!!',
				text: 'Testing some Mailgun awesomeness!'
			};

			sendMail.messages().send(data, function (error, body) {
				console.log(body);
			});

			return response.json({
				success: true
			});

		} catch (e) {
			return response.json({
				success: false,
				error: e.getMessage()
			});
		}
	});
}