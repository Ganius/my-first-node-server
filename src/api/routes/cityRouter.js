import {Router} from 'express';
import CityService from "../../services/CityService";

const route = Router();

export default app => {
  app.use('/city', route);

  route.post('/', async (request, response) => {
    const {name, country, population} = request.body;

    const city = await CityService.createCity({
      name, country, population
    });

    return response.json({
      success: true,
      data: city
    })
  });
}