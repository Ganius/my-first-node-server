import '@babel/polyfill';

import express from 'express';
import appLoader from './loaders';
import config from './config';

async function startServer() {
	const app = express();

	await appLoader({expressApp: app});

	app.listen(config.PORT, err => {
		if (err) {
			process.exit(1);
			return;
		}

		console.log(`
        ################################################
        Server listening on port: ${ config.PORT }
        🛡️################################################`);
	});
}

startServer();



