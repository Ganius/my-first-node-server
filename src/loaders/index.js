import expressLoader from "./express";
import {connectToMongoDb} from './mongodb';

export default async ({expressApp}) => {

	await connectToMongoDb();
	console.log('MongoDb is connected and loaded');

	await expressLoader({ app: expressApp });
	console.log('✌ Express loaded');
};