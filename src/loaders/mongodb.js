import mongoose from 'mongoose';
import config from '../config';

/**
 * @desc З'єднання з БД
 * @return {Promise}
 */

export async function connectToMongoDb() {
const connection = await mongoose
  .connect(
    config.MONGODB_URI, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useUnifiedTopology: true
    });

  return connection.connection.db;
}