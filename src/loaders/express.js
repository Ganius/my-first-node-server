import express from 'express';
import routes from '../api';
import bodyParser from 'body-parser';
import cors from 'cors';

const allowedOrigins = [
	'http://localhost:3000'
];


export default ({app}) => {
	app.set('view engine', 'pug');
	app.set('views', 'src/views');
	app.use('/static', express.static(__dirname + '/../public'));

	app.use(cors({
		origin: function (origin, callback) {
			// allow requests with no origin
			// (like mobile apps or curl requests)
			if (!origin) {
				return callback(null, true);
			}
			if (allowedOrigins.indexOf(origin) === -1) {
				const msg = 'The CORS policy for this site does not allow access from the specified Origin.';
				return callback(new Error(msg), false);
			}
			return callback(null, true);
		},
		credentials: true
	}));

	//Middleware that transforms the raw string of req.body into json
	app.use(bodyParser.json());

	// Load API routes
	app.use('/api', routes());

	/// catch 404 and forward to error handler
	app.use((req, res, next) => {
		const err = new Error('Not Found');
		err['status'] = 404;
		next(err);
	});

/// error handlers
	app.use((err, request, response, next) => {
		const {
			status = 500,
			name = '',
			message: error = 'Internal Application Error'
		} = err;
		const customResponse = response.status(500).json({
			status: 500,
			success: false,
			error
		});
		return name === 'UnauthorizedError'
			? customResponse.end()
			: customResponse;
	});
}