import UserModel from './user.model';
import CityModel from './city.model'

export {
  UserModel,
  CityModel
};