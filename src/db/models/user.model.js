import mongoose from 'mongoose';

// create a schema
const User = new mongoose.Schema({
  email: String,
  password: String,
  name: String,
  salt: String,
  allowNewsletters: Boolean,
  termsAccepted: Boolean,
  status: String,
  created_at: Date,
  updated_at: Date
});

export default mongoose.model('User', User);