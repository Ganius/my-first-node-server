import {CityModel} from '../db/models';

export default class CityService {
  /**
   * @desc створення користувача
   * @param {String} name
   * @param {String} country
   * @param {Number} population
   * @return {Promise}
   */
  static async createCity({name, country, population}) {
    return CityModel.create({
      name,
      country,
      population
    })
  }
}