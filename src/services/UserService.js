import {UserModel} from '../db/models';
import jwt from 'jsonwebtoken';
import config from '../config';

export default class UserService {
  /**
   * @param userId {String}
   * @return {Promise<*>}
   */
  static async getUserById(userId) {
    return UserModel.findById(userId);
  }

  /**
   * @desc створення користувача
   * @param {String} name
   * @param {String} email
   * @param {String} password
   * @return {Promise}
   */
  static async createUser({name, email, password}) {
    const user = await UserModel.create({
      email, name
    });

    const today = new Date();
    const exp = new Date(today);
    exp.setDate(today.getDate() + 60);

    const token = jwt.sign(
      {
        _id: user._id,
        name: user.name,
        exp: exp.getTime() / 1000,
      },
      config.JWT_SECRET,
    );
    return {
      ...user.toObject(),
      token
    }
  }
}